/**
 * @author waar0003
 */

/*
 * Definition of result type datastructures. Each code is divided into different types, which is designated by the first number of the code:
 * 1xx: Informational - Request received, continuing process
 * 2xx: Success - The action was successfully received, understood, and accepted
 * 3xx: Redirection - Further action must be taken in order to complete the request
 * 4xx: Client Error - The request contains bad syntax or cannot be fulfilled
 * 5xx: Server Error - The server failed to fulfill an apparently valid request
 */
var typeInfo = {
	name: "Informational",
	description: "Request received, continuing process"
};

var typeSuccess = {
	name: "Success",
	description: "The action was successfully received, understood, and accepted"
};

var typeRedir = {
	name: "Redirection",
	description: "Further action must be taken in order to complete the request"
};

var typeClientError = {
	name: "Client Error",
	description: "The request contains bad syntax or cannot be fulfilled"
};

var typeServerError = {
	name: "Server Error",
	description: "The server failed to fulfill an apparently valid request"
};

var typeUserError = {
	name: "You put in an invalid number",
	description:"Please try again with a different number"
};

/**
 * Returns a datastructure that explains the specified response code, or NULL is the code is not recognized
 * @param {Object} responseCode the response code that should be explained
 * @return a datastructure that explains the specified response code, or NULL is the code is not recognized
 */
function SetUp() {
	var UserInput= document.getElementById('ResponseInput').value;
	var UserNumber = parseInt(UserInput);		//Turns the the input of the user from a string to an integer
	getHTTPResponseInfo(UserNumber);

};

function getHTTPResponseInfo(responseCode) {
	//console.log(responseCode);
	var result = {};
	result.code = responseCode;
	if (responseCode === 100){
		result.type = typeInfo;
		result.description = "Continue";
	}
	else if (responseCode === 101){
		result.type = typeInfo;
		result.description = "Switching Protocols";
	}
	else if (responseCode === 102){
		result.type = typeInfo;
		result.description = "Processing";
	}
	else if (responseCode === 200 ) {
		result.type = typeSuccess;
		result.description = "OK";
	}
	else if (responseCode === 201){
		result.type = typeSuccess;
		result.description = "Created";
	}
	else if (responseCode === 202){
		result.type = typeSuccess;
		result.description = "Accepted";
	}
	else if (responseCode === 203){
		result.type = typeSuccess;
		result.description = "Non-Authorative Information";
	}
	else if (responseCode === 204){
		result.type = typeSuccess;
		result.description = "No Content";
	}
	else if (responseCode === 205){
		result.type = typeSuccess;
		result.description = "Reset Content";
	}
	else if (responseCode === 206){
		result.type = typeSuccess;
		result.description = "Partial Content";
	}
	else if (responseCode === 207){
		result.type = typeSuccess;
		result.description = "Multi-Status";
	}
	else if (responseCode === 208){
		result.type = typeSuccess;
		result.description = "Already Reported";
	}
	else if (responseCode === 226){
		result.type = typeSuccess;
		result.description = "IM Used";
	}
	else if (responseCode === 300){
		result.type = typeRedir;
		result.description = "Multiple Choices";
	}
	else if (responseCode === 301){
		result.type = typeRedir;
		result.description = "Moved Permanently";
	}
	else if (responseCode === 302){
		result.type = typeRedir;
		result.description = "Found";
	}
	else if (responseCode === 303){
		result.type = typeRedir;
		result.description = "See other";
	}
	else if (responseCode === 304){
		result.type = typeRedir;
		result.description = "Not Modified";
	}
	else if (responseCode === 305){
		result.type = typeRedir;
		result.description = "Use Proxy";
	}
	else if (responseCode === 306){
		result.type = typeRedir;
		result.description = "Switch Proxy";
	}
	else if (responseCode === 307){
		result.type = typeRedir;
		result.description = "Temporary Redirect";
	}
	else if (responseCode === 308){
		result.type = typeRedir;
		result.description = "Permanent Redirect or Resume Incomplete";
	}
	else if (responseCode === 400){
		result.type = typeClientError;
		result.description = "Bad Request";
	}
	else if (responseCode === 401){
		result.type = typeClientError;
		result.description = "Unauthorized";
	}
	else if (responseCode === 402){
		result.type = typeClientError;
		result.description = "Payment Required";
	}
	else if (responseCode === 403 ) {
		result.type = typeClientError;
		result.description = "Forbidden";
	}
	else if (responseCode === 404){
		result.type = typeClientError;
		result.description = "Not Found";		
	}
	else if (responseCode === 405){
		result.type = typeClientError;
		result.description = "Method Not Allowed";
	}
	else if (responseCode === 406){
		result.type = typeClientError;
		result.description = "Not Acceptable";
	}
	else if (responseCode === 407){
		result.type = typeClientError;
		result.description = "Proxy Authentication Required";
	}
	else if (responseCode === 408){
		result.type = typeClientError;
		result.description = "Request Timeout";
	}
	else if (responseCode === 409){
		result.type = typeClientError;
		result.description = "Conflict";
	}
	else if (responseCode === 410){
		result.type = typeClientError;
		result.description = "Gone";
	}
	else if (responseCode === 411){
		result.type = typeClientError;
		result.description = "Length Required";
	}
	else if (responseCode === 412){
		result.type = typeClientError;
		result.description = "Preconditon Failed";
	}
	else if (responseCode === 413){
		result.type = typeClientError;
		result.description = "Payload Too Large";
	}
	else if (responseCode === 414){
		result.type = typeClientError;
		result.description = "Requested-URL Too Long";
	}
	else if (responseCode === 415){
		result.type = typeClientError;
		result.description = "Unsupported Media Type";
	}
	else if (responseCode === 416){
		result.type = typeClientError;
		result.description = "Requested Range Not Satisfiable";
	}
	else if (responseCode === 417){
		result.type = typeClientError;
		result.description = "Expectation Failed";
	}
	else if (responseCode === 418){
		result.type = typeClientError;
		result.description = "I'm a teapot";
	}
	else if (responseCode === 419){
		result.type = typeClientError;
		result.description = "Authentication Timeout";
	}
	else if(responseCode === 420){
		result.type = typeClientError;
		result.description = "Method Failure or Enhance Your Calm";
	}
	else if (responseCode === 421){
		result.type = typeClientError;
		result.description = "Misdirected Request";
	}
	else if (responseCode === 422){
		result.type = typeClientError;
		result.description = "Unprocessable Entity";
	}
	else if (responseCode === 423){
		result.type = typeClientError;
		result.description = "Locked";
	}
	else if (responseCode === 424){
		result.type = typeClientError;
		result.description = "Failed Dependency";
	}
	else if (responseCode === 426){
		result.type = typeClientError;
		result.description = "Upgrade Required";
	}
	else if (responseCode === 428){
		result.type = typeClientError;
		result.description = "Precondition Required";
	}
	else if (responseCode === 429){
		result.type = typeClientError;
		result.description = "Too Many Request";
	}
	else if (responseCode === 431){
		result.type = typeClientError;
		result.description = "Request Header Fields Too Large";
	}
	else if (responseCode === 440){
		result.type = typeClientError;
		result.description = "Login Timeout";
	}
	else if (responseCode === 444){
		result.type = typeClientError;
		result.description = "No Response";
	}
	else if (responseCode === 449){
		result.type = typeClientError;
		result.description = "Retry With";
	}
	else if (responseCode === 450){
		result.type = typeClientError;
		result.description = "Blocked by Windows Parental Controls";
	}
	else if (responseCode === 451){
		result.type = typeClientError;
		result.description = "Unavailable For Legal Reasons or Redirected";
	}
	else if (responseCode === 494){
		result.type = typeClientError;
		result.description = "Request Header Too Large";
	}
	else if (responseCode === 495){
		result.type = typeClientError;
		result.description = "Cert Error";
	}
	else if (responseCode == 496){
		result.type = typeClientError;
		result.description ="No Cert";
	}
	else if (responseCode === 497){
		result.type = typeClientError;
		result.description = "HTTP to HTTPS";
	}
	else if (responseCode === 498){
		result.type = typeClientError;
		result.description = "Token expired/invalid";
	}
	else if (responseCode === 499){
		result.type = typeClientError;
		result.description = "Token required";
	}
	else if (responseCode === 500){
		result.type = typeServerError;
		result.description ="Internal Server Error";
	}
	else if (responseCode === 501){
		result.type = typeServerError;
		result.description ="Not Implemented";
	}
	else if (responseCode === 502){
		result.type = typeServerError;
		result.description ="Bad Gateway";
	}
	else if (responseCode === 503){
		result.type = typeServerError;
		result.description ="Service Unavailable";
	}
	else if (responseCode === 504){
		result.type = typeServerError;
		result.description ="Gateway Timeout";
	}
	else if (responseCode === 505){
		result.type = typeServerError;
		result.description ="HTTP Version Not Supported";
	}
	else if (responseCode === 506){
		result.type = typeServerError;
		result.description ="Variant Also Negotiates";
	}
	else if (responseCode === 507){
		result.type = typeServerError;
		result.description ="Insufficient Storage";
	}
	else if (responseCode === 508){
		result.type = typeServerError;
		result.description ="Loop Detected";
	}
	else if (responseCode === 509){
		result.type = typeServerError;
		result.description ="Bandwidth Limit Exceeded";
	}
	else if (responseCode === 510){
		result.type = typeServerError;
		result.description ="Not Expected";
	}
	else if (responseCode === 511){
		result.type = typeServerError;
		result.description ="Network Authentication Required";
	}
	else if (responseCode === 520){
		result.type = typeServerError;
		result.description ="Unknown Error";
	}
	else if (responseCode === 522){
		result.type = typeServerError;
		result.description ="Origin Connection Time-out";
	}
	else if (responseCode === 598){
		result.type = typeServerError;
		result.description ="Network read timeout error";
	}
	else if (responseCode === 599){
		result.type = typeServerError;
		result.description ="Network connect timeout error";
	}
	
	
	else{
		result.type = typeUserError;
		result.description = "This number doesn't work";
	}
	
	document.getElementById("StartCode").innerHTML = result.code;
	document.getElementById("OutputType").innerHTML = result.type.name;
	document.getElementById("OutputDesc").innerHTML = result.type.description;
	document.getElementById('OutputBrowser').innerHTML = result.description;
	return result;
};